/*public class Swap2{
public static void main(String[]args){
int a=5,b=6,c=7,d=8;
a=a+b+c+d;
a=a-(b+c+d);
b=a-();
c=a+b-d;
d=a+b-c;
System.out.println(d);
System.out.println(a);
System.out.println(b);
System.out.println(c);
}
}*/
public class Swap2 {
    public static void main(String[] args) {
        int a = 5, b = 6, c = 7, d = 8;
        // Applying the swap logic to achieve a = 8, b = 5, c = 6, d = 7
        a = a + b + c + d; // a = 5 + 6 + 7 + 8 = 26
        b = a - (b + c + d); // b = 26 - (6 + 7 + 8) = 5
        c = a - (b + c + d); // c = 26 - (5 + 7 + 8) = 6
        d = a - (b + c + d); // d = 26 - (5 + 6 + 8) = 7
        a = a - (b + c + d); // a = 26 - (5 + 6 + 7) = 8
        System.out.println("a = " + a); // Output: a = 8
        System.out.println("b = " + b); // Output: b = 5
        System.out.println("c = " + c); // Output: c = 6
        System.out.println("d = " + d); // Output: d = 7
    }
}

/*public class SwapValues {
    public static void main(String[] args) {
        // Initial values
        int a = 5;
        int b = 6;
        int c = 7;
        int d = 8;
        // Swapping the values
        int tempA = d;
        int tempB = a;
        int tempC = b;
        int tempD = c;
        a = tempA;
        b = tempB;
        c = tempC;
        d = tempD;
		// Printing the swapped values
        System.out.println("a = " + a + ", b = " + b + ", c = " + c + ", d = " + d);
    }
}




public class Swap2 {
    public static void main(String[] args) {
        int a = 5, b = 6, c = 7, d = 8;
        a = a + b + c + d; // a becomes the sum of all four variables
        a = a - (b + c + d); // a becomes the initial value of a (5)
        b = a + b + c + d - (a + c + d); // b becomes the initial value of b (6)
        c = a + b + c + d - (a + b + d); // c becomes the initial value of c (7)
        d = a + b + c + d - (a + b + c); // d becomes the initial value of d (8)
        System.out.println("a = " + a); // Output: a = 5
        System.out.println("b = " + b); // Output: b = 6
        System.out.println("c = " + c); // Output: c = 7
        System.out.println("d = " + d); // Output: d = 8
    }
}*/

