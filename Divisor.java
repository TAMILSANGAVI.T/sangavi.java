/*public class Divisor
{
public static void main(String[]args)
{
int i=1;
while(i<100)
{i++;
}
if(100%i==0){
System.out.println(i);
}
}
}*/

public class Divisor{
    public static void main(String[] args) {
        int number = 100;
        int i = 2;

        System.out.println("Divisors of " + number + " are:");
        while (i <= number) {
            if (number % i == 0) {
                System.out.println(i);
            }
            i++;
        }
    }
}
